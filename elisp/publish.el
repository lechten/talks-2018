;;; publish.el --- Publish reveal.js presentation from Org files on Gitlab Pages
;; -*- Mode: Emacs-Lisp -*-
;; -*- coding: utf-8 -*-

;; Copyright (C) 2017, 2018 Jens Lechtenbörger

;;; License: GPLv3

;;; Commentary:
;; Inspired by publish.el by Rasmus:
;; https://gitlab.com/pages/org-mode/blob/master/publish.el


;;; Code:
(package-initialize)
(require 'org)
(require 'ox-publish)
(require 'ox-html)

(setq debug-on-error t)
(message "Org-version: %s" org-version)

(setq org-export-with-smart-quotes t
      org-confirm-babel-evaluate nil
      org-descriptive-links nil)

(add-to-list 'load-path
	     (expand-file-name
	      "../emacs-reveal/" (file-name-directory load-file-name)))
(require 'reveal-config)
(setq org-reveal-root "./reveal.js")

;; Following colors are based on tango custom theme.
(custom-set-faces
 '(default                      ((t (:foreground "#2e3436"))))
 '(font-lock-builtin-face       ((t (:foreground "#75507b"))))
 '(font-lock-comment-face       ((t (:foreground "#5f615c"))))
 '(font-lock-constant-face      ((t (:foreground "#204a87"))))
 '(font-lock-function-name-face ((t (:bold t :foreground "#a40000"))))
 '(font-lock-keyword-face       ((t (:foreground "#346604"))))
 '(font-lock-string-face        ((t (:foreground "#5c3566"))))
 '(font-lock-type-face          ((t (:foreground "#204a87"))))
 '(font-lock-variable-name-face ((t (:foreground "#b35000"))))
 )

;; MathJax is huge.  So far, I don't need it; hence, this and the relevant
;; section below are commented out.
;; Besides, Gitlab supports KaTeX, which I might try later.
;; (setq org-reveal-mathjax-url "./MathJax/MathJax.js?config=TeX-AMS-MML_HTMLorMML")

(setq org-latex-pdf-process
      '("latexmk -outdir=%o -interaction=nonstopmode -shell-escape -bibtex -pdf %f")
      org-publish-project-alist
      (list
       (list "talks"
	     :base-directory "."
	     :base-extension "org"
	     :exclude "index\\|config\\|course-list\\|license\\|backmatter"
	     :publishing-function '(org-reveal-publish-to-reveal
				    org-latex-publish-to-pdf
				    )
	     :publishing-directory "./public"
	     )
       (list "images"
	     :base-directory "figures"
	     :base-extension (regexp-opt '("png" "jpg" "ico" "svg" "gif"))
	     :publishing-directory "./public/figures"
	     :publishing-function 'org-publish-attachment
	     :recursive t)
       (list "title-slide"
	     :base-directory "emacs-reveal/title-slide"
	     :base-extension (regexp-opt '("png" "jpg"))
	     :publishing-directory "./public/title-slide/"
	     :publishing-function 'org-publish-attachment)
       (list "reveal-static"
	     :base-directory "emacs-reveal/reveal.js"
	     :exclude "\\.git"
	     :base-extension 'any
	     :publishing-directory "./public/reveal.js"
	     :publishing-function 'org-publish-attachment
	     :recursive t)
       (list "reveal-theme"
	     :base-directory "emacs-reveal/css"
	     :base-extension 'any
	     :publishing-directory "./public/reveal.js/css/theme"
	     :publishing-function 'org-publish-attachment)
       (list "other-themes"
	     :base-directory "css"
	     :base-extension 'any
	     :publishing-directory "./public/reveal.js/css/theme"
	     :publishing-function 'org-publish-attachment)
       (list "reveal-toc-plugin"
	     :base-directory "emacs-reveal/Reveal.js-TOC-Progress/plugin"
	     :base-extension 'any
	     :publishing-directory "./public/reveal.js/plugin"
	     :publishing-function 'org-publish-attachment
	     :recursive t)
       (list "reveal.js-plugins-anything"
	     :base-directory "emacs-reveal/reveal.js-plugins/anything"
	     :base-extension 'any
	     :publishing-directory "./public/reveal.js/plugin/anything"
	     :publishing-function 'org-publish-attachment
	     :recursive t)
       (list "reveal.js-plugins-audio-slideshow"
	     :base-directory "emacs-reveal/reveal.js-plugins/audio-slideshow"
	     :base-extension 'any
	     :publishing-directory "./public/reveal.js/plugin/audio-slideshow"
	     :publishing-function 'org-publish-attachment
	     :recursive t)
       (list "reveal.js-jump-plugin"
	     :base-directory "emacs-reveal/reveal.js-jump-plugin/jump"
	     :base-extension 'any
	     :publishing-directory "./public/reveal.js/plugin/jump"
	     :publishing-function 'org-publish-attachment
	     :recursive t)
       ;; (list "mathjax-static"
       ;; 	     :base-directory "../emacs-reveal/MathJax"
       ;; 	     :exclude "\\.git"
       ;; 	     :base-extension 'any
       ;; 	     :publishing-directory "./public/MathJax"
       ;; 	     :publishing-function 'org-publish-attachment
       ;; 	     :recursive t)
       (list "site" :components '("talks"))))

(provide 'publish)
;;; publish.el ends here
